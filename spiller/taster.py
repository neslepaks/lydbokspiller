import board
from digitalio import DigitalInOut, Direction, Pull


class SpillerTaster:
    def __init__(self):
        self.buttons = {
            'A': DigitalInOut(board.D5),
            'B': DigitalInOut(board.D6),
            'L': DigitalInOut(board.D27),
            'R': DigitalInOut(board.D23),
            'U': DigitalInOut(board.D17),
            'D': DigitalInOut(board.D22),
            'C': DigitalInOut(board.D4)
        }
        for button in self.buttons:
            self.buttons[button].direction = Direction.INPUT
            self.buttons[button].pull = Pull.UP

    def get_input(self):
        return [button for button in self.buttons
                if self.buttons[button].value == False]
