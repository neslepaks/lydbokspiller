import fcntl
import logging
import os
import re
from subprocess import Popen, PIPE

import natsort

SKIP_INTERVAL = 30

class SpillerAudio:
    def __init__(self):
        self.paused = False
        self.process = False

    def play(self, bookdir):
        self.files = natsort.natsorted(bookdir.glob('*'), key=lambda x: x.name)
        cmd = ['mplayer', '-quiet', '-slave', '-nolirc', '-vo', 'null', '-ao', 'alsa'] + self.files
        self.process = Popen(cmd, stdout=PIPE, stdin=PIPE)
        flags = fcntl.fcntl(self.process.stdout, fcntl.F_GETFL)
        fcntl.fcntl(self.process.stdout, fcntl.F_SETFL, flags | os.O_NONBLOCK)
        self.current_fileno = 1
        logging.info('Running {}: {}'.format(self.process, ' '.join(map(str, cmd))))

    def is_alive(self):
        if not self.process:
            return False
        if self.process.poll() is not None:
            logging.info('Process {} no longer alive, reaping'.format(self.process))
            self.process.wait()
            self.process = False
            return False
        return True

    def pause(self):
        if not self.is_alive():
            return
        self.send('pause')
        logging.info('Paused {}'.format(self.process))
        self.paused = True

    def unpause(self):
        if not self.is_alive():
            return
        self.send('pause')
        logging.info('Resumed {}'.format(self.process))
        self.paused = False

    def is_paused(self):
        return self.is_alive() and self.paused

    def stop(self):
        if not self.is_alive():
            return
        self.send('quit')
        self.process.wait()
        logging.info('Stopped {}'.format(self.process))
        self.process = False

    def send(self, msg):
        if not self.is_alive():
            return
        self.process.stdin.write((msg + '\n').encode())
        self.process.stdin.flush()

    def skip(self):
        if not self.is_alive():
            return
        if self.process:
            self.send(f'seek {SKIP_INTERVAL}')
            logging.info('Skipped {} seconds'.format(SKIP_INTERVAL))

    def get_file_progress(self):
        if not self.is_alive():
            return None
        try:
            chunk = os.read(self.process.stdout.fileno(), 1024)
            m = re.search(r'Playing (.*)\.\n', chunk.decode())
            if m:
                filenames = list(map(str, self.files))
                self.current_fileno = filenames.index(m.group(1)) + 1
        except OSError:
            pass
        return '{}/{}'.format(self.current_fileno, len(self.files))
