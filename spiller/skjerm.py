import textwrap

from board import SCL, SDA
import busio
from PIL import Image, ImageDraw, ImageFont
import adafruit_ssd1306

from .volum import MAX_VOLUME


class SpillerSkjerm:
    def __init__(self):
        i2c = busio.I2C(SCL, SDA)
        self.oled = adafruit_ssd1306.SSD1306_I2C(128, 64, i2c)

        # Clear display.
        self.oled.fill(0)
        self.oled.show()

        # Create blank image for drawing.
        self.image = Image.new("1", (self.oled.width, self.oled.height))
        self.draw = ImageDraw.Draw(self.image)

        # Load a font we've installed
        self.font = ImageFont.truetype("/usr/share/fonts/truetype"
                                       "/dejavu/DejaVuSans.ttf", 13)
        self.smallfont = ImageFont.truetype("/usr/share/fonts/truetype"
                                            "/dejavu/DejaVuSans.ttf", 10)


    def clear(self):
        self.oled.fill(0)
        self.oled.show()

    def update(self, title, playing, volume, file_progress):
        self.draw.rectangle((0, 0, self.oled.width, self.oled.height),
                            outline=0, fill=0)
        offset = 0
        for line in textwrap.wrap(title, 14):
            self.draw.text((0, offset), line, font=self.font, fill=255)
            offset += 12
        if file_progress is not None:
            self.draw.text((0, 53), file_progress, font=self.smallfont, fill=255)
        self.draw.rectangle((120, 25, 127, 33), fill=1)
        if playing:
            self.draw.rectangle((120, 55, 122, 63), fill=1)
            self.draw.rectangle((125, 55, 127, 63), fill=1)
        else:
            self.draw.polygon([(120, 55), (127, 59), (120, 63)], fill=1)

        #self.draw.rectangle((0, 63, 0, 63-(volume//(MAX_VOLUME/63.0))), fill=1)

        self.oled.image(self.image)
        self.oled.show()
