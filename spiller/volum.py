import logging
import re
import subprocess

DEFAULT_DELTA=10
MAX_VOLUME=50

class SpillerVolum:
    def __init__(self):
        self.get_volume()

    def get_volume(self):
        p = subprocess.check_output(['amixer', '-M'])
        r = re.search(r"Simple mixer control '(.+?)',0", p.decode())
        self.dev = r.group(1)
        r = re.search(r'Mono:.*?\[(\d+)%\]', p.decode())
        self.pct = int(r.group(1))
        if self.pct > MAX_VOLUME:
            self.decrease(delta=-(self.pct-MAX_VOLUME))

    def increase(self, delta=DEFAULT_DELTA):
        new = max(0, min(MAX_VOLUME, self.pct + delta))
        logging.info('Volume changing from {} to {}'.format(self.pct, new))
        p = subprocess.check_output(['amixer', '-M', 'set',
                                     self.dev, f'{new}%'])
        r = re.search(r'Mono:.*?\[(\d+)%\]', p.decode())
        self.pct = int(r.group(1))

    def decrease(self, delta=-DEFAULT_DELTA):
        self.increase(delta)
