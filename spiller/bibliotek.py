from pathlib import Path


class SpillerBibliotek:
    def __init__(self, library_name):
        self.library = Path.home() / library_name
        self.catalog = {}
        for bookdir in self.library.iterdir():
            display_name = str(bookdir.name).upper().replace('_', ' ')
            self.catalog[display_name] = bookdir

    def first(self):
        return next(iter(self.catalog))

    def last(self):
        return list(iter(self.catalog))[-1]

    def next(self, current):
        catiter = iter(self.catalog)
        for name in catiter:
            if name == current:
                break
        try:
            return next(catiter)
        except StopIteration:
            return self.first()

    def prev(self, current):
        prev = False
        for name, bookdir in self.catalog.items():
            if name == current:
                if prev:
                    return prev
                else:
                    return self.last()
            prev = name

    def path(self, name):
        return self.catalog[name]
