A Raspberry Pi based audiobook player for my kid
================================================

This is a small python project that I made for my 6 year old.  It turns a Raspberry Pi with an [Adafruit OLED bonnet](https://learn.adafruit.com/adafruit-128x64-oled-bonnet-for-raspberry-pi/) into a simple audiobook player.

![Lydbokspiller](https://imgur.com/v4mxxya.jpg)

Hardware used
-------------

- A Raspberry Pi 3 model A+
- An OLED bonnet from Adafruit: https://learn.adafruit.com/adafruit-128x64-oled-bonnet-for-raspberry-pi/

Requirements
------------

```
sudo apt install mplayer python3-pip
python3 -mpip install -r requirements.txt
```

You also need to go into `raspi-config` and enable I2C under Interfaces. Reboot afterwards.

Usage
-----

Copy some audio files into subdirectories of ~/Lydbøker.  Subdirectory names get upcapsed and underscore replaced to space so `alice_in_wonderland` will read as ALICE IN WONDERLAND on the display.  The files can be called anything as long as the natural sorting order makes sense.  I use FLACs, but presumably anything mplayer can play will work.

To make it come right up on boot I add something like this to /etc/rc.local:

```
su pi sh -c 'cd ~/lydbokspiller && python3 lydbokspiller.py ~/Lydbøker' &
```

Controls
--------

Up and down on the stick is volume, left and right is book selection.  Pressing down on it skips ahead 30 seconds.  The other two buttons are stop and play/pause.
