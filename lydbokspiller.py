import logging
import logging.handlers
import sys
import time

from spiller.volum import SpillerVolum
from spiller.skjerm import SpillerSkjerm
from spiller.taster import SpillerTaster
from spiller.bibliotek import SpillerBibliotek
from spiller.audio import SpillerAudio

INACTIVITY_SECONDS = 60

logging.basicConfig(handlers=[logging.handlers.SysLogHandler("/dev/log")],
                    format="%(asctime)s %(levelname)s %(message)s",
                    level=logging.DEBUG)


class Lydbokspiller:
    def __init__(self):
        self.volume = SpillerVolum()
        self.screen = SpillerSkjerm()
        self.input = SpillerTaster()
        self.library = SpillerBibliotek('Lydbøker')
        self.audio = SpillerAudio()

        self.current_book = self.library.first()

        self.activity = time.time()
        self.screensaver = False
        self.playing = False

    def start(self):
        self.update()

        last_file_progress = None
        while True:
            time.sleep(.1)
            pressed = self.input.get_input()

            if self.playing and not self.audio.is_alive():
                self.playing = False

            if not pressed:
                current_file_progress = self.audio.get_file_progress()
                if current_file_progress != last_file_progress:
                    if not self.screensaver:
                        self.update()
                    last_file_progress = current_file_progress

                if not self.screensaver and (time.time() - INACTIVITY_SECONDS >
                                             self.activity):
                    self.screensaver = True
                    self.screen.clear()
                continue

            self.activity = time.time()

            if self.screensaver:
                self.screensaver = False
                self.update()
                continue

            # Multiple keys may be pressed at once, but usually only one action
            # is valid, so handle them ordered by decreasing importance.

            if 'R' in pressed:
                self.current_book = self.library.next(self.current_book)
            elif 'L' in pressed:
                self.current_book = self.library.prev(self.current_book)
            elif 'U' in pressed:
                self.volume.increase()
            elif 'D' in pressed:
                self.volume.decrease()
            elif 'A' in pressed:
                if self.playing:
                    self.playing = False
                    self.audio.pause()
                else:
                    self.playing = True
                    if self.audio.is_paused():
                        self.audio.unpause()
                    else:
                        self.audio.play(self.library.path(self.current_book))
            elif 'B' in pressed:
                self.playing = False
                self.audio.stop()
            elif 'C' in pressed:
                self.audio.skip()

            self.update()

            # After executing an action from input, pause a little to avoid
            # accidental double-presses.
            time.sleep(.2)

    def update(self):
        self.screen.update(self.current_book,
                           self.playing,
                           self.volume.pct,
                           self.audio.get_file_progress())

if __name__ == '__main__':
    spiller = Lydbokspiller()
    try:
        spiller.start()
    except Exception as e:
        spiller.screen.clear()
        spiller.audio.stop()
        raise
